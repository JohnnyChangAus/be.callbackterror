### Porject Name CT Academy (CT stands for Callback Terror)

### Update notes
### `27 Nov`
install new package:
    * http
    * koa-static
    * path
    * fs
    * koa2

### Get Started with BitBucket

Welcome, lovely members !!
The following steps will help you to initialize you Bitbucket. And you will understand : how to push a file, set up an SSH key, and send a pull request.
Contact me if having any issues. Cheers !!

1. Check the inbox from the email you provided for my invatation of project3 from bitbucket.

2. Fill in your name which MUST be matched to you name at our WeChat group

3. Go to repositories then select BE.CallbackTerror

4. Back to your laptop/desktop, make sure Git has been installed to your local. visit https://git-scm.com/downloads if it hasn't been installed.


5. Set up an SSH key to skip login bitbucket when you push to remote repository
    https://support.atlassian.com/bitbucket-cloud/docs/set-up-an-ssh-key/

6. Find a preferred path from your laptop/desktop, and new a folder, named project3

7. Locate the path to project3 from command line. It might look like this:
    $username\documents\project3

8. Use **git clone** to clone the repository to your local
    git clone https://JohnnyChangAus@bitbucket.org/JohnnyChangAus/be.callbackterror.git

9. perform each commands below:
    * cd be.callbackterror
    * git init
    * git branch [dev]
    * git branch [yourname]
    * git checkout [yourname]
    * echo "push test by [yourname]" > test.md
    * git add test.md
    * git commit -m "push test by [yourname]"
    * git push origin [yourname]

10. Back to remote repository page, switch branch from master to [yourname] by drop down list.

11. Check for test.md that you just pushed does exist

12. click **Pull requests** on left menu

13. then click button **Create pull request** on top-right

14. Make sure your branch name is on the left and development on the right
    ** All pull requests must send to "development" branch ! **

15. Scoll down and click "Create pull request" when ready

16.  Pull request (PR) pratice sending:
    * Edit readme.md from development branch and fill the date and [yourname]
    * then send a PR as above step 10 to 15.
    * Make sure you edit the latest readme.md to avoid conflict
    * Send a PR everyday at least once until Sat 17 October.

### Conflict happens
When conflict happens, your pull request won't be accepted by team lead until conflict solved.  


It must be discussed with who you conflicted with, then refer the steps below to fix conflict  


![image](conflict.png)

### `12 Oct` Today's topic
1. read this https://bitbucket.org/JohnnyChangAus/be.callbackterror/src/master/
2. complete to send a pull request
3. deal with it if conflict happens

### `13 Oct` Today's topic  

1. Edit readme.md from your own branch as following require :

2. Insert a new line like, My name is [yourname]. I'm the [order] one to edit this file today. My favorite fruit is [fruit].
    * The order must be different from other members'
    * Name a fruit which is different from other members'
    * The text in [] must be displayed as emphasized like **this**.
    * commit message "13 Oct 2020 Today's topic from whom"

3. Complete to send a pull request without conflict.

4. deal with it if conflict happens

### `14 Oct` Today's topic  

Discuss with where you conflicted including the different orders and fruits and deal with it.

### `15 Oct` Today's topic  

Update your pull request in selected order to avoid conflict.

### `16 Oct` Today's topic  
    * Complete form https://forms.gle/UK4eBYBa611MD6yQA by the end of day
    * read workflow chart and database diagram
    * create a new js file from your branch and build a arrow function that can calculate sum (a + b) without error.

### `17 Oct` Today's topic  
    * Continue yesterday's topic, send a pull request for building a button (including css) to perform the function when on click and display the result.
    * Take notes of your commonds or questions regarding our workflow and database diagram.
    
## 以後群組討論出現的時間統一為Sydney Time

### Porject Name CT Academy (CT stands for Callback Terror)

### Get Started with BitBucket

Welcome, lovely members !!
The following steps will help you to initialize you Bitbucket. And you will understand : how to push a file, set up an SSH key, and send a pull request.
Contact me if having any issues. Cheers !!

1. Check the inbox from the email you provided for my invatation of project3 from bitbucket.

2. Fill in your name which MUST be matched to you name at our WeChat group

3. Go to repositories then select BE.CallbackTerror

4. Back to your laptop/desktop, make sure Git has been installed to your local. visit https://git-scm.com/downloads if it hasn't been installed.


5. Set up an SSH key to skip login bitbucket when you push to remote repository
    https://support.atlassian.com/bitbucket-cloud/docs/set-up-an-ssh-key/

6. Find a preferred path from your laptop/desktop, and new a folder, named project3

7. Locate the path to project3 from command line. It might look like this:
    $username\documents\project3

8. Use **git clone** to clone the repository to your local
    git clone https://JohnnyChangAus@bitbucket.org/JohnnyChangAus/be.callbackterror.git

9. perform each commands below:
    * cd be.callbackterror
    * git init
    * git branch [dev]
    * git branch [yourname]
    * git checkout [yourname]
    * echo "push test by [yourname]" > test.md
    * git add test.md
    * git commit -m "push test by [yourname]"
    * git push origin [yourname]

10. Back to remote repository page, switch branch from master to [yourname] by drop down list.

11. Check for test.md that you just pushed does exist

12. click **Pull requests** on left menu

13. then click button **Create pull request** on top-right

14. Make sure your branch name is on the left and development on the right
    ** All pull requests must send to "development" branch ! **

15. Scoll down and click "Create pull request" when ready

16.  Pull request (PR) pratice sending:
    * Edit readme.md from development branch and fill the date and [yourname]
    * then send a PR as above step 10 to 15.
    * Make sure you edit the latest readme.md to avoid conflict
    * Send a PR everyday at least once until Sat 17 October.

### Conflict happens
When conflict happens, your pull request won't be accepted by team lead until conflict solved.  


It must be discussed with who you conflicted with, then refer the steps below to fix conflict  


![image](conflict.png)

### `12 Oct` Today's topic
1. read this https://bitbucket.org/JohnnyChangAus/be.callbackterror/src/master/
2. complete to send a pull request
3. deal with it if conflict happens

### `13 Oct` Today's topic  

1. Edit readme.md from your own branch as following require :

2. Insert a new line like, My name is [yourname]. I'm the [order] one to edit this file today. My favorite fruit is [fruit].
    * The order must be different from other members'
    * Name a fruit which is different from other members'
    * The text in [] must be displayed as emphasized like **this**.
    * commit message "13 Oct 2020 Today's topic from whom"

3. Complete to send a pull request without conflict.

4. deal with it if conflict happens

### `14 Oct` Today's topic  

Discuss with where you conflicted including the different orders and fruits and deal with it.

### `15 Oct` Today's topic  

Update your pull request in selected order to avoid conflict.

### `16 Oct` Today's topic  
    * Complete form https://forms.gle/UK4eBYBa611MD6yQA by the end of day
    * read workflow chart and database diagram
    * create a new js file from your branch and build a arrow function that can calculate sum (a + b) without error.

### `17 Oct` Today's topic  
    * Continue yesterday's topic, send a pull request for building a button (including css) to perform the function when on click and display the result.
    * Take notes of your commonds or questions regarding our workflow and database diagram.