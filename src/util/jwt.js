require('dotenv').config()

const jwt = require('jsonwebtoken')
const { createTextChangeRange } = require('typescript')
const secretToken = process.env.TOKEN_SECRET
console.log(secretToken)


//generate jwt token
const jwtSign = (data) => {
    const token = jwt.sign(data, secretToken, { expiresIn: '12h' });
    return token;
}

const jwtVerify = (token, ctx) => {
    
    if(token !== 'null') {
        jwt.verify(token, secretToken, (err, decoded) => {
            if (err) {
                ctx.status = 401
                console.log(err)
            } else {
                ctx.status = 200
                ctx.body = decoded
            }
        })
    } else {
        ctx.status = 401
    }

}

module.exports = {
    jwtSign,
    jwtVerify,
}
