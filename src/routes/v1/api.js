const { router } = require("../../loaders");
const courseControllers = require("../../controllers/course");
const studentControllers = require("../../controllers/student");
const userControllers = require("../../controllers/user");
const scheduleControllers = require('../../controllers/schedule')

const checkDuplicate = require('../../middleware/user/checkDuplicate')
const checkEmailChange = require('../../middleware/user/checkEmailChange')

router.get('/courses', courseControllers.index); // look up all
router.get('/course/:id', courseControllers.show); // look up one
router.post('/course', courseControllers.store); // create one
router.put('/course/:id', courseControllers.update); // update one
router.delete('/course/:id', courseControllers.destroy); // delete one
router.get("/courses/:page/:pageSize", courseControllers.showPagination)
router.put('/course/:id/:student', courseControllers.addStudent)
router.patch('/course/:id/:student', courseControllers.removeStudent)

router.get('/students', studentControllers.index);
router.get('/student/:id', studentControllers.show);
router.post('/student', studentControllers.store);
router.patch('/student/:id', studentControllers.update);
router.delete('/student/:id', studentControllers.destroy);

router.post("/user/login", userControllers.validate)
router.get("/user/validateroot", userControllers.validateRoot)
router.post("/user", checkDuplicate, userControllers.create)
router.put("/user/:email", checkEmailChange, userControllers.update)
router.delete("/user/:email",  userControllers.destroy)

router.get('/schedules', scheduleControllers.index); // look up all
router.get('/schedule/:id', scheduleControllers.show); // look up one
router.post('/schedule', scheduleControllers.create); // create one
router.put('/schedule/:id', scheduleControllers.update); // update one
// router.delete('/course/:id', scheduleControllers.destroy); // delete one


module.exports.routers = router;
