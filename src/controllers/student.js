const mongoose = require('mongoose');
const Student = require('../models/Student');

//get all Students
exports.index = async (ctx) => {
    const res = await Student.find({});
    ctx.body = res;
};

//show 1 Student
exports.show = async (ctx) => {
    const { id } = ctx.params;
    const res = await Student.findOne({ _id: new mongoose.Types.ObjectId(id) });
    if (res) {
        ctx.body = res;
    } else {
        ctx.status = 404;
        ctx.body = {
            message: `${id} cannot be found`,
        }
    }
};

//create Student
exports.store = async (ctx) => {
    const { body } = ctx.request;
    const student = new Student(body);
    const { _id } = await student.save();
    ctx.status = 201;
    ctx.body = {
        message: "Student credited",
        _id,
    };
};

//update Student
exports.update = async (ctx) => {
    const { id } = ctx.params;
    const { body } = ctx.request;
    const { prefer_name } = body;

    const { n } = await Student.updateOne(
        { _id: new mongoose.Types.ObjectId(id) }, {
        $set: body,
    });

    if (n === 0) {
        ctx.body = {
            message: `${id} not found!`,
        };
        ctx.status = 404;
    } else {
        ctx.status = 200;
        ctx.body = {
            message: `Student: ${id} updated`,
        };
    }
};

//delete Student
exports.destroy = async (ctx) => {
    const { id } = ctx.params;
    const { n } = await Student.deleteOne({ _id: id });
    if (n === 0) {
        ctx.body = {
            message: `${id} not found!`,
        };
        ctx.status = 404;
    } else {
        ctx.status = 200;
    }
};
