const mongoose = require('mongoose');
const { exists } = require('../models/course');
const Course = require('../models/course');
const Student = require('../models/Student');

//get all courses
exports.index = async (ctx) => {
    let res = await Course.find({}).populate('students');
    ctx.body = res;

};

//show 1 course
exports.show = async (ctx) => {
    const { id } = ctx.params;
    const res = await Course.findOne({ _id: new mongoose.Types.ObjectId(id) });
    if (res) {
        ctx.body = res;
    } else {
        ctx.status = 404;
        ctx.body = {
            message: `${id} cannot be found`,
        }
    }
};

exports.showPagination = async (ctx) => {
    const { keyword } = ctx.query

    let { page, pageSize } = ctx.params
    page = +page
    pageSize = +pageSize

    //const {age} = ctx.query

    const skipCount = (page - 1) * pageSize
    if (keyword === "") {
        const total = await Course.find().countDocuments()
        //skip() query next time skip count
        //const users = await User.find().skip(skipCount).limit(pageSize)
        //const users = await Course.find().select("_id course_name").skip(skipCount).limit(pageSize)
        const courses = await Course.find().sort({ createdAt: -1 }).skip(skipCount).limit(pageSize)
        ctx.body = {
            total,
            results: courses,
        }
    } else {
        const total = await Course.find({ $or: [{ course_name: { $regex: keyword, $options: "i" } }, { aims: { $regex: keyword, $options: "i" } }] }).countDocuments()
        //skip() query next time skip count
        //const users = await User.find().skip(skipCount).limit(pageSize)
        //const users = await Course.find().select("_id course_name").skip(skipCount).limit(pageSize)
        const courses = await Course.find({ $or: [{ course_name: { $regex: keyword, $options: "i" } }, { aims: { $regex: keyword, $options: "i" } }] }).sort({ createdAt: -1 }).skip(skipCount).limit(pageSize)

        ctx.body = {
            total,
            results: courses,
        }
    }

}

//create course
exports.store = async (ctx) => {
    const { body } = ctx.request;
    const { course_name } = body;
    if (Course[course_name]) {
        ctx.status = 400;
        ctx.body = {
            message: `Course Name: ${course_name} already exists!`
        }
    } else {
        const course = new Course(body);
        const { _id } = await course.save();
        ctx.status = 201;
        ctx.body = {
            message: `Course: ${course_name} created`,
            _id,
        };
    }

};

//update course
exports.update = async (ctx) => {
    const { id } = ctx.params;
    const { body } = ctx.request;
    const { course_name } = body;

    const { n } = await Course.updateOne(
        { _id: new mongoose.Types.ObjectId(id) }, {
        $set: body,
    });

    if (n === 0) {
        ctx.body = {
            message: `${id} not found!`,
        };
        ctx.status = 404;
    } else {
        ctx.status = 200;
        ctx.body = {
            message: `Course: ${course_name} updated`,
        };
    }
};

//delete course
exports.destroy = async (ctx) => {
    const { id } = ctx.params;

    const { n } = await Course.deleteOne({ _id: new mongoose.Types.ObjectId(id) });
    if (n === 0) {
        ctx.body = {
            message: `${id} not found!`,
        };
        ctx.status = 404;
    } else {
        ctx.status = 200;
    }
};

// add a student to a course
exports.addStudent = async (ctx) => {
    const { id, student } = ctx.params;
    const course = await Course.findOne({ _id: id })
    const student1 = await Student.findOne({ _id: student })
    // course.students.push(student1._id)
    if (course.students.indexOf(student1._id) === -1) {
        course.students.push(student1._id)
        await course.save();
        await student1.updateOne({course: `${course._id}`})
        // student.course.push(course._id)
        // await student.save();
        ctx.status = 200
        ctx.body = {
            message: `${student1.prefer_name} joint ${course.course_name}`
        }
    } else {
        ctx.status = 400
        ctx.body = {
            message: 'student exists',
            body: [course.course_name, student1.prefer_name]
        }
    }
}

// remove a student from a course
exports.removeStudent = async (ctx) => {
    const { id, student } = ctx.params;
    const course = await Course.findOne({ _id: id })
    const student1 = await Student.findOne({ _id: student })
    // course.students.push(student1._id)
    if (course.students.indexOf(student1._id) === -1) {
        ctx.body = {
            message: `${student1.prefer_name} is not in the course`,
            body: [course.course_name, student1.prefer_name]
        }
    } else {
        course.students.pop(student1._id)
        await course.save();
        await student1.updateOne({course: ""})
        ctx.body = {
            message: `${student1.prefer_name} removed from ${course.course_name}`
        }
    }
}