const mongoose = require('mongoose');
const Student = require('../models/student');

//get all students
exports.index =  (ctx) => {
    ctx.body = "Student";
};

//show 1 student
exports.show = (ctx) => {

};

//create student
exports.store = (ctx) => {
    let {body} = ctx.request;
    const student = new Student(body);

    student.save();
    ctx.status = 200;
    ctx.body = {message: student};
};

//update student
exports.update = (ctx) => {
};

//delete student
exports.destroy = (ctx) => {

};
