const mongoose = require('mongoose');
const Course = require('../models/course');
const Schedule = require('../models/schedule')

// get all schedules
exports.index = async (ctx) => {
    const res = await Schedule.find({});
    ctx.body = res;
};

// get 1 schedule
exports.show = async (ctx) => {
    const { id } = ctx.params;
    const res = await Schedule.findOne({ _id: new mongoose.Types.ObjectId(id) });
    if (res) {
        ctx.body = res;
    } else {
        ctx.status = 404;
        ctx.body = {
            message: 'cannot be found'
        }
    }
};

// create 1 schedule
exports.create = async (ctx) => {
    const { body } = ctx.request;
    const schedule = new Schedule(body);
    const { _id } = await schedule.save();
    ctx.status = 201;
    ctx.body = {
        message: 'New schedule created',
        _id,
    }
};

// update 1 schedule
exports.update = async (ctx) => {
    const { id } = ctx.params;
    const { body } = ctx.request;
    const { _id } = body;
    const { n } = await Schedule.updateOne(
        { _id: new mongoose.Types.ObjectId(_id) }, {
        $set: body,
    });

    if (n === 0) {
        ctx.body = {
            message: 'not found'
        };
        ctx.status = 404;
    } else {
        ctx.status = 200;
        ctx.body = {
            message: 'schedule updated'
        };
    };
};

// delete 1 schedule
exports.destroy = async (ctx) => { };
