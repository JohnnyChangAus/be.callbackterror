const mongoose = require('mongoose');
const User = require('../models/User');
const bcrypt = require("bcrypt");
const { jwtSign, jwtVerify } = require("../util/jwt")

//validate login
exports.validate = async (ctx) => {
    const {body} = ctx.request
    const res = await User.findOne({email: body.email})
    if (res) {
        
        const isPwdValid = bcrypt.compareSync(body.password, res.password)
        if(isPwdValid) {
            const token = jwtSign({...res})
            ctx.status = 200
            ctx.body = {
                token,
                message: "Log in successfully",
            }
        } else {
            ctx.status = 401
            ctx.body = {
                message: "Wrong password"
            }
        }
    } else {
        ctx.status = 404
        ctx.body = {
            message: `email not found`
        }
    }
}

exports.validateRoot = (ctx) => {
    
    const token = ctx.headers['x-access-token']
    jwtVerify(token, ctx)

}

//create user
exports.create = async (ctx) => {
    const {body} = ctx.request
    const hashedBody = {...body, password: bcrypt.hashSync(body.password, 10)}
    const user = new User(hashedBody)
    const {_id} = await user.save()

    ctx.status = 201
    ctx.body = {
        message: "user created",
        id: _id,
    }
}

//update user
exports.update = async (ctx) => {
    const { email } = ctx.params
    const { body } = ctx.request
    if (body.password === "" || body.password === null) {
        hashedBody = body;
    } else {
        hashedBody = { ...body, password: bcrypt.hashSync(body.password, 10) }
    }
    const { n } = await User.updateOne({ email: email },
        {
            $set: hashedBody
        })
    if (n === 0) {
        ctx.body = {
            message: `${email} not found!`
        }
        ctx.status = 404
    } else {
        ctx.body = {
            message: `${email} updated!`
        }
        ctx.status = 200
    }
}

//delete user
exports.destroy = async (ctx) => {
    const {email} = ctx.params
    console.log(email)
    const {n} = await User.deleteOne({email: email})
    console.log(n)
    if (n === 0) {
        ctx.body = {
            message: `${email} not found!`
        }
        ctx.status = 404
    } else {
        ctx.body = {
            message: `${email} deleted`
        }
        ctx.status = 200
    }
}
