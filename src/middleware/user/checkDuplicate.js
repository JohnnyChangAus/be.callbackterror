const User = require('../../models/User')

module.exports = async function checkDuplicate(ctx, next) {
    const {body} = ctx.request
    try {
        const isDuplicate = await User.findOne({email: body.email})
        console.log(isDuplicate )

        if (isDuplicate) {
            ctx.body = {
                message: "email exists"
            }
            ctx.status = 422
            return
        }
        
    } catch (e) {
        ctx.body = {
            message: e.message
        }
        ctx.status = 400
        return
    }
    await next()
}