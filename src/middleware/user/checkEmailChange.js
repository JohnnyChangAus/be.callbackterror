module.exports = async (ctx, next) => {
    const {email} = ctx.params
    const {body} = ctx.request
    if (email !== body.email && body.email !== undefined) {
        ctx.body = {
            message: `email cannot be modified`
        }
        ctx.status = 401
        return 
    }
    await next()
}