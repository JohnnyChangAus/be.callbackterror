const mongoose = require('mongoose');

module.exports = async function () {
    // connect to MongoDB Atlas
    const connection = await mongoose.connect('mongodb+srv://aussiept:aussiept@cluster0.ulpwk.mongodb.net/aussiept?retryWrites=true&w=majority', {
        useNewUrlParser: true,
        useUnifiedTopology: true,
        useCreateIndex: true,
        useFindAndModify: false
    });

    // connect to local MongoDB
    // const connection = await mongoose.connect('mongodb://127.0.0.1:27017/?compressors=disabled&gssapiServiceName=mongodb', {
    //     useNewUrlParser: true,
    //     useUnifiedTopology: true,
    // });

    return connection.connection.db;
};
