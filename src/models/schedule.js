const mongoose = require('mongoose');

const ScheduleSchema = new mongoose.Schema(
    {
        date: {
            type: String,
            required: false,
            trim: true,
        },
        time_start: {
            type: String,
            required: false,
            trim: true,
        },
        time_finish: {
            type: String,
            required: false,
            trim: true,
        },
        student_id: {
            type: String,
            required: false,
            trim: true,
        },
        course_id: {
            type: String,
            required: false,
            trim: true,
        },
        active: {
            type: Boolean,
            require: false,
            trim: true,
        },
        delete: {
            type: Boolean,
            require: false,
            trim: true,
        }

    }, {timestamps: true},
);


const Schedule = mongoose.model('Schedule', ScheduleSchema);
module.exports = Schedule;