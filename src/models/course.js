const mongoose = require('mongoose');

const courseSchema = new mongoose.Schema(
    {
        course_name: {
            type: String,
            required: true,
        },
        duration: {
            type: String,
            required: true,
            trim: true,
        },
        aims: {
            type: String,
            required: true,
            trim: true,
        },
        description: {
            type: String,
            required: true,
            trim: true,
        },
        course_name: {
            type: String,
            required: true,
            trim: true,
        },
        duration_: {
            type: String,
            required: true,
            trim: true,
        },
        price: {
            type: Number,
            required: true,
            trim: true,
        },
        type: {
            type: String,
            required: false,
            trim: true,
            enum: ['main', 'mormal'],
        },
        students: [{
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Student'
        }]

    }, {timestamps: true},
);


const Course = mongoose.model('Course', courseSchema);
module.exports = Course;
