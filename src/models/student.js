const mongoose = require('mongoose');

const StudentSchema = new mongoose.Schema(
    {
        prefer_name: {
            type: String,
            required: true,
            trim: true,
        },
        first_name: {
            type: String,
            required: true,
            trim: true,
        },
        last_name: {
            type: String,
            required: true,
            trim: true,
        },
        email: {
            type: String,
            required: false,
            trim: true,
        },
        mobile: {
            type: String,
            required: false,
            trim: true,
        },
        address: {
            type: String,
            required: false,
            trim: true,
        },
        height: {
            type: Number,
            required: false,
            trim: true,
            min: 1,
            max: 250,
        },
        gender: {
            type: String,
            required: false,
            trim: true,
            enum: ['male', 'female'],
        },
        year_of_birth: {
            type: Number,
            required: false,
            trim: true,
            min: 1900,
            max: 2020,
        },
        course: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Course'
        }

    }, {timestamps: true},
);

const Student = mongoose.model('Student', StudentSchema);
module.exports = Student;