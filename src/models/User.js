const mongoose = require('mongoose');

const UserSchema = new mongoose.Schema(
    {
        email: {
            type: String,
            required: true,
            trim: true,
        },
        password: {
            type: String,
            required: true,
            trim: true,
        },
        first_name: {
            type: String,
            required: false,
            trim: true,
        },
        last_name: {
            type: String,
            required: false,
            trim: true,
        },
        mobile: {
            type: String,
            required: false,
            trim: true,
        },
        location: {
            type: String,
            required: false,
            trim: true,
        },
        about_me: {
            type: String,
            required: false,
            trim: true,
        },
        instagram: {
            type: String,
            required: false,
            trim: true,
        },
        youtube: {
            type: String,
            required: false,
            trim: true,
        },
    }, {timestamps: true}
)

const User = mongoose.model('User', UserSchema)
module.exports = User
